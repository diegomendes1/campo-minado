﻿using UnityEngine;
public class Campo{

	private int numBombas;
	private int x, y;
	private Casa[,] tabuleiro;

	public Campo(int numBombas, int x, int y){
		this.numBombas = numBombas;
		this.x = x;
		this.y = y;
	}

	public void GerarTabuleiro(Casa[,] novoTabuleiro){
		if(novoTabuleiro != null){
			this.tabuleiro = novoTabuleiro;
			return;
		}
		this.tabuleiro = new Casa[x,y];

		for(int i = 0; i < x; i++){
			for(int j = 0; j < y; j++){
				tabuleiro[i,j] = new Casa();
			}
		}

		int bombasQueFaltam = numBombas;

		while(bombasQueFaltam > 0){
			int randomX = Random.Range(0, x), randomY = Random.Range(0, y);
			if(!tabuleiro[randomX, randomY].GetIsBomba()){
				tabuleiro[randomX, randomY].SetIsBomba();
				bombasQueFaltam--;
			}
		}

		for(int i = 0; i < x; i++){
			for(int j = 0; j < y; j++){
				if(!tabuleiro[i,j].GetIsBomba()){
					tabuleiro[i,j].SetNumBombasRedor(EncontrarValorCasa(i, j));
				}
			}
		}
	}

	public int EncontrarValorCasa(int x, int y){
		int resultado = 0;

		//cima esquerda
		if(VerificarExisteBomba(x-1, y-1)){
			resultado++;
		}

		//cima direita
		if(VerificarExisteBomba(x-1, y+1)){
			resultado++;
		}

		//cima
		if(VerificarExisteBomba(x-1, y)){
			resultado++;
		}

		//baixo esquerda
		if(VerificarExisteBomba(x+1, y-1)){
			resultado++;
		}

		//baixo direita
		if(VerificarExisteBomba(x+1, y+1)){
			resultado++;
		}

		//baixo
		if(VerificarExisteBomba(x+1, y)){
			resultado++;
		}

		//esquerda
		if(VerificarExisteBomba(x, y-1)){
			resultado++;
		}

		//direita
		if(VerificarExisteBomba(x, y+1)){
			resultado++;
		}

		

		return resultado;
	}

	private bool VerificarExisteBomba(int x, int y){
		if(x >= 0 && x < this.x && y >= 0 && y < this.y){
			if(tabuleiro[x,y].GetIsBomba()){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	public Casa[,] GetTabuleiro(){
		return this.tabuleiro;
	}

	public Casa GetCasa(int x, int y){
		return this.tabuleiro[x, y];
	}

	public int GetX(){
		return this.x;
	}

	public int GetY(){
		return this.y;
	}

	public void MostrarTabuleiroConsole(){
		string print = "";
		for(int i = 0; i < x; i++){
			for(int j = 0; j < y; j++){
				if(tabuleiro[i,j].GetIsBomba()){
					print += " X ";
				}else{
					print += " " + tabuleiro[i,j].GetNumBombasRedor() + " ";
				}
				
			}
			Debug.Log(print);
			print = "";
		}
	}
}

﻿public class Casa{
	private bool isBomba = false;
	private bool isBandeira = false;
	private int numBombasRedor = 0;
	private bool isVisivel = false;
	private bool verificadoVazio = false;
	
    public void SetIsBomba(){
		this.isBomba = true;
	}

	public bool GetIsBomba(){
		return this.isBomba;
	}

	public void SetNumBombasRedor(int numBombasRedor){
		this.numBombasRedor = numBombasRedor;
	}

	public int GetNumBombasRedor(){
		return this.numBombasRedor;
	}

	public bool GetIsVisivel(){
		return this.isVisivel;
	}

	public void SetIsVisivel(){
		this.isVisivel = true;
	}

	public void SetHide(){
		this.isVisivel = false;
	}

	public bool GetVerificadoVazio(){
		return this.verificadoVazio;
	}

	public void SetVerificadoVazio(){
		this.verificadoVazio = true;
	}

	public void SetNaoVerificadoVazio(){
		this.verificadoVazio = false;
	}

	public bool GetIsBandeira(){
		return this.isBandeira;
	}

	public void ColocarBandeira(){
		this.isBandeira = true;
	}

	public void RetirarBandeira(){
		this.isBandeira = false;
	}
}

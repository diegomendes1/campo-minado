﻿using UnityEngine;
using UnityEngine.UI;

public class CasaUI : MonoBehaviour {
	private Sprite casaEscondidaIMG;
	private Sprite casaVisivelIMG;
	private Sprite casaBandeiraIMG;
	private int posX = 0, posY = 0;

	public void SetCoordenadas(int posX, int posY){
		this.posX = posX;
		this.posY = posY;
	}

	public int GetPosX(){
		return this.posX;
	}

	public int GetPosY(){
		return this.posY;
	}

	public void SetSprites(Sprite escondidaIMG, Sprite visivelIMG, Sprite bandeiraIMG){
		this.casaEscondidaIMG = escondidaIMG;
		this.casaVisivelIMG = visivelIMG;
		this.casaBandeiraIMG = bandeiraIMG;
	}
	public void EscolherSpriteEscondido(){
		Image img = GetComponent<Image>();
		img.sprite = casaEscondidaIMG;
	}

	public void EscolherSpriteVisivel(){
		Image img = GetComponent<Image>();
		img.sprite = casaVisivelIMG;
	}

	public void EscolherSpriteBandeira(){
		Image img = GetComponent<Image>();
		img.sprite = casaBandeiraIMG;
	}

	public void SetSprite(Sprite novoSprite){
		Image img = GetComponent<Image>();
		img.sprite = novoSprite;
	}
}

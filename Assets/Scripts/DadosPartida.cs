﻿using UnityEngine;

public class DadosPartida : MonoBehaviour {
	private int numBombas;
	private int tamanhoX;
	private int tamanhoY;
	private Casa[,] tabuleiro;
	[SerializeField]
	private Tema temaAtual;

	public void AdicionarDetalhes(int numBombas, int tamanhoX, int tamanhoY){
		this.numBombas = numBombas;
		this.tamanhoX = tamanhoX;
		this.tamanhoY = tamanhoY;
	}

	public int GetNumBombas(){
		return this.numBombas;
	}

	public int GetTamanhoX(){
		return this.tamanhoX;
	}

	public int GetTamanhoY(){
		return this.tamanhoY;
	}

	public Casa[,] GetTabuleiro(){
		return this.tabuleiro;
	}

	public void SetTabuleiro(Casa[,] tabuleiro){
		this.tabuleiro = tabuleiro;
	}

	public Tema GetTemaAtual(){
		return this.temaAtual;
	}

	public void SetTemaAtual(Tema tema){
		this.temaAtual = tema;
	}
}

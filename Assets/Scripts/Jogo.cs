﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Jogo : MonoBehaviour {
	private Campo campo;
	[SerializeField]
	private Tela tela;
	private bool usandoBandeira = false;
	[SerializeField]
	private int totalBandeiras;
	private DadosPartida dados;

	void Start(){
		dados = GameObject.Find("DadosPartida").GetComponent<DadosPartida>();
		IniciarJogo(dados.GetNumBombas(), dados.GetTamanhoX(), dados.GetTamanhoY());
	}

	private void IniciarJogo (int numBombas, int x, int y) {
		tela.EncontrarTema(dados.GetTemaAtual());
		totalBandeiras = numBombas;
		this.campo = new Campo(numBombas, x, y);
		campo.GerarTabuleiro(dados.GetTabuleiro());
		tela.mostrarTabuleiroInicial(campo.GetTabuleiro(), x, y);
		tela.OrganizarMenus();
		//campo.MostrarTabuleiroConsole();
		tela.AtualizarNumBandeiras(totalBandeiras);
	}

	public void ExecutarJogada(CasaUI casa, int x, int y){
		if(usandoBandeira){
			if(campo.GetCasa(x,y).GetIsBandeira()){
				tela.EsconderCasa(casa);
				campo.GetCasa(x,y).RetirarBandeira();
				totalBandeiras++;
				tela.AtualizarNumBandeiras(totalBandeiras);
			}else{
				if(!campo.GetCasa(x,y).GetIsVisivel() && totalBandeiras > 0){
					tela.MostrarBandeira(casa);
					campo.GetCasa(x,y).ColocarBandeira();
					totalBandeiras--;
					tela.AtualizarNumBandeiras(totalBandeiras);
				}
			}
		}else{
			if(campo.GetCasa(x,y).GetIsBomba()){
				tela.MostrarCasa(casa);
				campo.GetCasa(x,y).SetIsVisivel();
				FimDeJogo();
				tela.MostrarBombaFimJogo(casa);
			}else{
				if(campo.GetCasa(x,y).GetNumBombasRedor() == 0){
					MostrarCasasVazias(campo.GetCasa(x,y), x, y);
				}else{
					tela.MostrarCasa(casa);
					campo.GetCasa(x,y).SetIsVisivel();
				}
			}
		}
	}

	public void MostrarCasasVazias(Casa casaAtual, int x, int y){
		if(casaAtual == null || casaAtual.GetVerificadoVazio()){
			return;
		}

		tela.MostrarCasa(tela.getCasaUI(x,y));
		campo.GetCasa(x,y).SetIsVisivel();
		if(casaAtual.GetIsBandeira()){
			totalBandeiras++;
			casaAtual.RetirarBandeira();
			tela.AtualizarNumBandeiras(totalBandeiras);
		}
		campo.GetCasa(x,y).SetVerificadoVazio();

		if(campo.GetCasa(x,y).GetNumBombasRedor() != 0){
			return;
		}

		//cima
		if(x > 0){
			if(!campo.GetCasa(x-1, y).GetIsVisivel()){
				MostrarCasasVazias(campo.GetCasa(x-1, y), x-1, y);
			}
		}
 
		//esquerda
		if(y > 0){
			if(!campo.GetCasa(x, y-1).GetIsVisivel()){
				MostrarCasasVazias(campo.GetCasa(x, y-1), x, y-1);
			}
		}
 
		//baixo
		if(x < campo.GetX() -1){
			if(!campo.GetCasa(x+1, y).GetIsVisivel()){
				MostrarCasasVazias(campo.GetCasa(x+1, y), x+1, y);
			}
		}

		//direita
		if(y < campo.GetY() -1){
			if(!campo.GetCasa(x, y+1).GetIsVisivel()){
				MostrarCasasVazias(campo.GetCasa(x, y+1), x, y+1);
			}
		}
	}

	public void TrocarUsoBandeira(){
		if(usandoBandeira){
			usandoBandeira = false;
		}else{
			usandoBandeira = true;
		}
	}

	public void FimDeJogo(){
		tela.GameOverMenu();
	}

	public void ReiniciarNovoJogo(){
		dados.SetTabuleiro(null);
		SceneManager.LoadScene(1);
	}

	public void ReiniciarMesmoJogo(){
		for(int i = 0; i < campo.GetX(); i++){
			for(int j = 0; j < campo.GetY(); j++){
				campo.GetCasa(i,j).SetHide();
				campo.GetCasa(i,j).SetNaoVerificadoVazio();
				campo.GetCasa(i,j).RetirarBandeira();
			}
		}
		dados.SetTabuleiro(campo.GetTabuleiro());
		SceneManager.LoadScene(1);
	}

	public Campo GetCampo(){
		return this.campo;
	}

	public bool GetUsandoBandeira(){
		return this.usandoBandeira;
	}

	public bool VerificarVitoria(){
		for(int i = 0; i < campo.GetX(); i++){
			for(int j = 0; j < campo.GetY(); j++){
				if(campo.GetCasa(i,j).GetIsBomba()){
					if(!campo.GetCasa(i,j).GetIsBandeira()){
						return false;
					}
				}else if(campo.GetCasa(i,j).GetIsBandeira()){
					return false;
				}
			}
		}
		return true;
	}
}
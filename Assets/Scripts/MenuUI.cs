﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class MenuUI : MonoBehaviour {
	[SerializeField]
	private Slider numBombasSlider;
	[SerializeField]
	private Text numBombasText;
	[SerializeField]
	private Slider tamanhoX;
	[SerializeField]
	private Text numTamanhoXText;
	[SerializeField]
	private Slider tamanhoY;
	[SerializeField]
	private Text numTamanhoYText;
	[SerializeField]
	private GameObject dadosPartida;
	[SerializeField]
	private Tema[] temas;
	[SerializeField]
	private GameObject temaMenu;


	public void Sair(){
		Application.Quit();
	}

	void Start(){
		DadosPartida dado = dadosPartida.AddComponent<DadosPartida>();
		if(dado.GetTemaAtual() == null){
			dado.SetTemaAtual(temas[0]);
		}
	}

	public void Iniciar(){
		DadosPartida dado = dadosPartida.GetComponent<DadosPartida>();
		dado.AdicionarDetalhes((int)numBombasSlider.value, (int)tamanhoX.value, (int)tamanhoY.value);
		DontDestroyOnLoad(dado);
		SceneManager.LoadScene(1);
	}

	public void AtualizarNumBombasText(){
		this.numBombasText.text = numBombasSlider.value.ToString();
	}

	public void AtualizarTamanhoXText(){
		this.numTamanhoXText.text = tamanhoX.value.ToString();
		this.numBombasSlider.maxValue = (tamanhoX.value * tamanhoY.value)/3;
	}

	public void AtualizarTamanhoYText(){
		this.numTamanhoYText.text = tamanhoY.value.ToString();
		this.numBombasSlider.maxValue = (tamanhoX.value * tamanhoY.value)/3;
	}

	public void FacilButton(){
		this.numBombasSlider.value = 5;
		this.tamanhoX.value = 5;
		this.tamanhoY.value = 5;
	}

	public void MedioButton(){
		this.numBombasSlider.value = 11;
		this.tamanhoX.value = 8;
		this.tamanhoY.value = 8;
	}

	public void DificilButton(){
		this.numBombasSlider.value = 16;
		this.tamanhoX.value = 10;
		this.tamanhoY.value = 10;
	}

	public void EscolherTemaOriginal(){
		dadosPartida.GetComponent<DadosPartida>().SetTemaAtual(temas[0]);

	}

	public void EscolherTemaWindows(){
		dadosPartida.GetComponent<DadosPartida>().SetTemaAtual(temas[1]);
	}

	public void EscolherTemaBeta(){
		dadosPartida.GetComponent<DadosPartida>().SetTemaAtual(temas[2]);
	}

	public void EntrarTemaMenu(){
		temaMenu.SetActive(true);
	}

	public void SairTemaMenu(){
		temaMenu.SetActive(false);
	}
}

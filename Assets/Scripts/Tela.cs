﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Tela : MonoBehaviour{
	[SerializeField]
	private GridLayoutGroup tabuleiroHolder;
	[SerializeField]
	private GameObject casaPrefab;
	[SerializeField]
	private Jogo jogo;
	[SerializeField]
	private Tema tema;
	private CasaUI[,] tabuleiroUI;
	[SerializeField]
	private GameObject gameOverMenu;
	[SerializeField]
	private GameObject trava;
	[SerializeField]
	private Animator bandeiraButton;
	[SerializeField]
	private Text bandeiraButtonText;
	[SerializeField]
	private Image bandeiraButtonIMG;
	[SerializeField]
	private Text numBandeirasText;
	[SerializeField]
	private GameObject prontoButton;
	[SerializeField]
	private Text gameOverText;

	public void EncontrarTema(Tema novoTema){
		if(novoTema == null){
			Debug.Log("teste");
		}
		tema.casaEscondidaIMG = novoTema.GetCasaEscondidaIMG();
		tema.casaBandeiraIMG = novoTema.GetCasaBandeiraIMG();
		tema.casaBombaIMG = novoTema.GetCasaBombaIMG();
		tema.casaNumeroIMG = novoTema.GetCasaNumeroArray();
		tema.modoBandeiraSprite = novoTema.GetModoBandeiraSprite();
		tema.modoCavarSprite = novoTema.GetModoCavarSprite();
		tema.bombaFimJogoSprite = novoTema.GetBombaFimJogoSprite();
		tema.casaBandeiraErradaIMG = novoTema.GetCasaBandeiraErradaIMG();

		bandeiraButtonIMG.sprite = tema.GetModoCavarSprite();
	}

	public void mostrarTabuleiroInicial(Casa[,] tabuleiro, int x, int y){
		tabuleiroUI = new CasaUI[x,y];
		tabuleiroHolder.GetComponent<RectTransform>().sizeDelta = new Vector2(40*y, 40*x);
		
		tabuleiroHolder.constraintCount = y;
		for(int i = 0; i < x; i++){
			for(int j = 0; j < y; j++){
				GameObject novaCasa = Instantiate(casaPrefab);
				CasaUI casaUI = novaCasa.GetComponent<CasaUI>();
				casaUI.SetCoordenadas(i,j);

				if(jogo.GetCampo().GetCasa(i,j).GetIsBomba()){
					casaUI.SetSprites(tema.GetCasaEscondidaIMG(),
									tema.GetCasaBombaIMG(),
									tema.GetCasaBandeiraIMG());
				}else{
					casaUI.SetSprites(tema.GetCasaEscondidaIMG(),
									tema.GetCasaNumeroIMG(jogo.GetCampo().GetCasa(i,j).GetNumBombasRedor()),
									tema.GetCasaBandeiraIMG());
				}

				EsconderCasa(casaUI);

				novaCasa.GetComponent<Button>().onClick.AddListener(delegate {
					jogo.ExecutarJogada(casaUI, casaUI.GetPosX(), casaUI.GetPosY());
					});

				novaCasa.transform.SetParent(tabuleiroHolder.transform);
				novaCasa.transform.localScale = new Vector3(1,1,1);

				tabuleiroUI[i,j] = casaUI;
			}
		}
	}

	public CasaUI getCasaUI(int x, int y){
		return tabuleiroUI[x,y];
	}

	public void BandeiraButton(){
		if(jogo.GetUsandoBandeira()){
			bandeiraButton.Play("BandeiraButtonModoCavar");
			bandeiraButtonText.text = "Eliminando Casas";
			bandeiraButtonIMG.sprite = tema.GetModoCavarSprite();
		}else{
			bandeiraButton.Play("BandeiraButtonModoBandeira");
			bandeiraButtonText.text = "Adicionando Bandeiras";
			bandeiraButtonIMG.sprite = tema.GetModoBandeiraSprite();
		}
		jogo.TrocarUsoBandeira();
	}



	public void VoltarMenu(){
		Destroy(GameObject.Find("DadosPartida"));
		SceneManager.LoadScene(0);
	}

	public void AumentarZoom(){
		if(tabuleiroHolder.transform.localScale.x < 2.5f){
		tabuleiroHolder.transform.localScale = new Vector3(tabuleiroHolder.transform.localScale.x +0.5f,
														tabuleiroHolder.transform.localScale.y +0.5f,
														tabuleiroHolder.transform.localScale.z +0.5f);
		}

		if(tabuleiroHolder.transform.localScale.x > 2.5f){
			tabuleiroHolder.transform.localScale = new Vector3(2.5f, 2.5f, 2.5f);
		}
	}

	public void DiminuirZoom(){
		if(tabuleiroHolder.transform.localScale.x > 0.5f){
		tabuleiroHolder.transform.localScale = new Vector3(tabuleiroHolder.transform.localScale.x -0.25f,
														tabuleiroHolder.transform.localScale.y -0.25f,
														tabuleiroHolder.transform.localScale.z -0.25f);
		}
	}

	public void GameOverMenu(){
		gameOverMenu.SetActive(true);
		trava.SetActive(true);
		LiberarTabuleiro();
		prontoButton.SetActive(false);
	}

	public void ReiniciarNovoJogo(){
		jogo.ReiniciarNovoJogo();
	}

	public void ReiniciarMesmoJogo(){
		jogo.ReiniciarMesmoJogo();
	}

	public void LiberarTabuleiro(){
		for(int i = 0; i < jogo.GetCampo().GetX(); i++){
			for(int j = 0; j < jogo.GetCampo().GetY(); j++){
				if(jogo.GetCampo().GetCasa(i,j).GetIsBandeira()){
					if(jogo.GetCampo().GetCasa(i,j).GetIsBomba()){
						MostrarCasa(tabuleiroUI[i,j]);
					}else{
						tabuleiroUI[i,j].SetSprite(tema.GetCasaBandeiraErradaIMG());
					}
				}else if(jogo.GetCampo().GetCasa(i,j).GetIsBomba()){
					MostrarCasa(tabuleiroUI[i,j]);
				}
			}
		}
	}

	public void OrganizarMenus(){
		gameOverMenu.SetActive(false);
		trava.SetActive(false);
	}

	public void MostrarCasa(CasaUI casa){
		casa.EscolherSpriteVisivel();
	}

	public void EsconderCasa(CasaUI casa){
		casa.EscolherSpriteEscondido();
	}

	public void MostrarBandeira(CasaUI casa){
		casa.EscolherSpriteBandeira();
	}

	public void MostrarBombaFimJogo(CasaUI casa){
		casa.GetComponent<Image>().sprite = tema.GetBombaFimJogoSprite();
	}

	public void AtualizarNumBandeiras(int x){
		numBandeirasText.text = "Restam " + x + " Bandeiras";
	}

	public void FinalizarJogo(){
		LiberarTabuleiro();
		if(jogo.VerificarVitoria()){
			gameOverText.text = "GANHOU! :)";
		}else{
			gameOverText.text = "PERDEU! :(";
		}
		GameOverMenu();
	}
}

﻿using UnityEngine;

public class Tema : MonoBehaviour {
	public Sprite casaEscondidaIMG;
	public Sprite casaBombaIMG;
	public Sprite casaBandeiraIMG;
	public Sprite[] casaNumeroIMG;
	public Sprite modoBandeiraSprite;
	public Sprite modoCavarSprite;
	public Sprite bombaFimJogoSprite;
	public Sprite casaBandeiraErradaIMG;

	public Sprite GetCasaBandeiraErradaIMG(){
		return this.casaBandeiraErradaIMG;
	}

	public Sprite GetCasaEscondidaIMG(){
		return this.casaEscondidaIMG;
	}

	public Sprite GetCasaBombaIMG(){
		return this.casaBombaIMG;
	}

	public Sprite GetCasaBandeiraIMG(){
		return this.casaBandeiraIMG;
	}

	public Sprite GetCasaNumeroIMG(int index){
		return this.casaNumeroIMG[index];
	}

	public Sprite[] GetCasaNumeroArray(){
		return this.casaNumeroIMG;
	}

	public Sprite GetModoCavarSprite(){
		return this.modoCavarSprite;
	}

	public Sprite GetModoBandeiraSprite(){
		return this.modoBandeiraSprite;
	}

	public Sprite GetBombaFimJogoSprite(){
		return this.bombaFimJogoSprite;
	}
}
